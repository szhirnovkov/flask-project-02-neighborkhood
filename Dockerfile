FROM python:3.9
WORKDIR /app
COPY requirements.txt /app
RUN pip install --upgrade pip 
RUN pip install -r requirements.txt
ENV FLASK_ENV production
EXPOSE 5000
COPY app/ /app/ 
CMD python3 __init__.py runserver 0.0.0.0:5000
